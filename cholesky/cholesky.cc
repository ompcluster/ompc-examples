#include <cassert>
#include <cblas.h>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <lapacke.h>
#include <limits>
#include <memory>
#include <omp.h>
#include <vector>

// export CC=clang
// export CXX=clang++
// make clean
// make
// mpirun -np 3 ./main 256 64

class BlockMatrix {
private:
  const int rowsPerBlock;
  const int colsPerBlock;
  const long nRows;
  const long nCols;
  const int nBlocksPerRow;
  const int nBlocksPerCol;
  std::vector<std::vector<std::unique_ptr<float[]>>> Blocks;

public:
  BlockMatrix(const int _rowsPerBlock, const int _colsPerBlock,
              const long _nRows, const long _nCols)
      : rowsPerBlock(_rowsPerBlock), colsPerBlock(_colsPerBlock), nRows(_nRows),
        nCols(_nCols), nBlocksPerRow(_nRows / _rowsPerBlock),
        nBlocksPerCol(_nCols / _colsPerBlock), Blocks(nBlocksPerCol) {
    for (int i = 0; i < nBlocksPerCol; i++) {
      for (int j = 0; j < nBlocksPerRow; j++) {
        Blocks[i].emplace_back(new float[_rowsPerBlock * _colsPerBlock]);
      }
    }
  };

  // Initialize the BlockMatrix from 2D arrays
  void Initialize(const std::vector<float> &matrix) {
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        float *CurrBlock = GetBlock(i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;
            CurrBlock[ii + jj * colsPerBlock] = matrix[curri + currj * nCols];
          }
      }
  }

  float *GetBlock(int i, int j) const {
    assert(i < nBlocksPerCol && j < nBlocksPerRow && "Accessing outside block");
    return Blocks[i][j].get();
  }

  // Print BlockMatrix
  void Print() {
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        float *CurrBlock = GetBlock(i, j);
        printf("Block (%d, %d) \n", i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii) {
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            printf(" %.5f", CurrBlock[ii * colsPerBlock + jj]);
          }
          printf("\n");
        }
        printf("\n");
      }
  }

  // Copy Final Results in matrix
  void Copy_Results(std::vector<float> &matrix) {
    for (int i = 0; i < nBlocksPerCol; i++)
      for (int j = 0; j < nBlocksPerRow; j++) {
        float *CurrBlock = GetBlock(i, j);
        for (int ii = 0; ii < colsPerBlock; ++ii)
          for (int jj = 0; jj < rowsPerBlock; ++jj) {
            int curri = i * colsPerBlock + ii;
            int currj = j * rowsPerBlock + jj;
            matrix[curri + currj * nCols] = CurrBlock[ii + jj * colsPerBlock];
          }
      }
  }
};

// Cholesky Factorization
void Cholesky_Factorization(BlockMatrix &A, long N, long BS) {
  #pragma omp parallel
  #pragma omp single
  {
    // Maps matrix´s blocks asynchronously (as tasks).
    for (int i = 0; i < N / BS; ++i) {
      for (int j = i; j < N / BS; ++j) {
        float *BlockA = A.GetBlock(j, i);
        #pragma omp target enter data nowait \
                    map(to: BlockA[:BS * BS]) \
                    depend(out: *BlockA)
      }
    }

    // Performs the Cholesky algorithm on the different blocks of the matrix.
    // The data is implicitly mapped and automatically moved by the data
    // manager.
    for (int k = 0; k < N / BS; k++) {
      float *Block_Diag = A.GetBlock(k, k);
      #pragma omp target nowait depend(inout: *Block_Diag) firstprivate(BS)
      LAPACKE_spotrf_work(LAPACK_COL_MAJOR, 'L', BS, Block_Diag, BS);

      for (int m = k + 1; m < N / BS; m++) {
        Block_Diag = A.GetBlock(k, k);
        float *Block_mk = A.GetBlock(m, k);
        #pragma omp target nowait firstprivate(BS) \
                                  depend(in: *Block_Diag) \
                                  depend(inout: *Block_mk)
        cblas_strsm(CblasColMajor, CblasRight, CblasLower, CblasTrans,
                    CblasNonUnit, BS, BS, 1.0, Block_Diag, BS, Block_mk, BS);
      }

      for (int m = k + 1; m < N / BS; m++) {

        float *Block_mk = A.GetBlock(m, k);
        float *Block_mm = A.GetBlock(m, m);
        #pragma omp target nowait firstprivate(BS) \
                                  depend(in: *Block_mk) \
                                  depend(inout: *Block_mm)
        cblas_ssyrk(CblasColMajor, CblasLower, CblasNoTrans, BS, BS, -1.0,
                    Block_mk, BS, 1.0, Block_mm, BS);

        for (int n = k + 1; n < m; n++) {
          float *Block_nk = A.GetBlock(n, k);
          float *Block_mn = A.GetBlock(m, n);
          #pragma omp target nowait firstprivate(BS) \
                                    depend(in: *Block_mk, *Block_nk) \
                                    depend(inout: *Block_mn)
          cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, BS, BS, BS, -1.0,
                      Block_mk, BS, Block_nk, BS, 1.0, Block_mn, BS);
        }
      }
    }

    // Removes matrix´s blocks and acquires the final result asynchronously.
    for (int i = 0; i < N / BS; ++i) {
      for (int j = i; j < N / BS; ++j) {
        float *BlockA = A.GetBlock(j, i);
        #pragma omp target exit data nowait \
                    map(from: BlockA[:BS * BS]) depend(inout: *BlockA)
      }
    }
  }
}

int main(int argc, char **argv) {
  double t;

  if (argc != 3) {
    fprintf(stderr, "[USAGE] %s dim nb )\n", argv[0]);
    fprintf(stderr, "  nb must divide dim\n");
    return 1;
  }
  const long N = atoi(argv[1]);
  const long BS = atoi(argv[2]);

  if (N % BS != 0) {
    fprintf(stderr, "[USAGE] %s dim nb \n", argv[0]);
    fprintf(stderr, "  nb must divide dim\n");

    return 1;
  }

  // Initialize matrix A considering that it must be positive symmetric.
  auto BlockedA = BlockMatrix(BS, BS, N, N);
  std::vector<float> a(N * N);

  int seed[] = {0, 0, 0, 1};
  LAPACKE_slarnv(1, seed, (size_t)N * N, &a[0]);

  for (int i = 0; i < N; i++) {
    a[i * N + i] = a[i * N + i] + N;
    for (int j = 0; j < i; j++) {
      a[j * N + i] = a[i * N + j];
    }
  }

  BlockedA.Initialize(a);
  fprintf(stdout, "<< Cholesky Factorization >>\n");
  t = omp_get_wtime();
  Cholesky_Factorization(BlockedA, N, BS);
  t = omp_get_wtime() - t;
  fprintf(stdout,
          "Offloaded Cholesky Factorization Computation done in %0.6lfs\n", t);

  // Compare our results
  std::vector<float> Aref(a);
  BlockedA.Copy_Results(a);
  LAPACKE_spotrf(LAPACK_COL_MAJOR, 'L', N, &Aref[0], N);
  float zmone = -1.0;
  cblas_saxpy((size_t)N * N, (zmone), &Aref[0], 1, &a[0], 1);
  float work[1];
  float Anorm =
      LAPACKE_slansy_work(LAPACK_COL_MAJOR, 'F', 'L', N, &Aref[0], N, work);
  float error =
      LAPACKE_slange_work(LAPACK_COL_MAJOR, 'F', N, N, &a[0], N, work);
  if (Anorm != 0)
    error /= Anorm;
  printf("The error of the Cholesky factorization calculation: %.5f\n", error);

  return 0;
}
