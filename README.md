OMPC Examples
================================================================================

This repository contains some examples of OMPC programs.

Container Image
--------------------------------------------------------------------------------

The easiest way to use OMPC is by downloading a Docker image from Dockerhub. The
image contains the compiler and all the necessary libraries to get started.
Every week an automated CI job creates and deploys a new `latest` image with the
most recent release of the OMPC runtime.


```console
$ docker pull ompcluster/runtime:latest
```

After the image is downloaded, you may navigate to your project directory and
instantiate a container from the image using the following command:


```console
$ docker run -it --rm -v $PWD:/root ompcluster/runtime:latest bash
```

To ensure the container is working properly, please should run `clang
--version` inside the container and check the output. It should look something
like this:

```
OmpCluster clang version 14.0.0 (git@gitlab.com:ompcluster/llvm-project.git 4f60767e575b6f97c269f5a3e66cc1d508d23d32)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /scratch/llvm/build-debug/bin
```

Compile and Run Programs
--------------------------------------------------------------------------------

The OMPC Runtime is built on top of the LLVM's OpenMP `libomptarget` interface.
Therefore, developers need our custom version of `clang` to compile programs.
The invocation is done as follows for C programs:

```console
$ clang -fopenmp -fopenm-targets=x86_64-pc-linux-gnu program.c -o program
```

And using `clang++` in case of C++ programs:

```console
$ clang++ -fopenmp -fopenm-targets=x86_64-pc-linux-gnu program.cpp -o program
```

Finally, after compiled, the program can be launched using MPI's utility
`mpirun` passing the number of processes as argument. Other flags of `mpirun`
can be used normally (*e.g.* `--hostfile`).

```console
$ mpirun -np 3 ./program
```

Further Information
--------------------------------------------------------------------------------

The OMPC Runtime source code lives at [Gitlab][gitlab]. If you encounter any
problems or misbehavior with the runtime please let us know by creating an
[issue][issues]. For a more complete reference check out our [Read the
Docs][rtdocs] page.


<!----------------------------------- LINKS ----------------------------------->
[gitlab]: https://gitlab.com/ompcluster/llvm-project
[issues]: https://gitlab.com/ompcluster/llvm-project/-/issues
[rtdocs]: https://ompcluster.readthedocs.io
